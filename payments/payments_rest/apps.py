from django.apps import AppConfig


class PaymentsRestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'payments_rest'
